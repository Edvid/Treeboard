import {SymbolActionPair} from "./SymbolActionPair.js"; 
import { KeyLegend } from "./KeyLegend.js";
import { InteractionTypeDispatcher } from "./InteractionTypeDispatcher.js";

export class KeyButton {

    static dist = 33;

    static repeaterFunc;

    static render(keyset, buttonNum, X, Y){

        let keyButtonContainer = document.createElement("div");

        keyButtonContainer.classList += "keybuttoncontainer";

        keyButtonContainer.style.top = `${Y * 100}%`
        keyButtonContainer.style.bottom = `${100 - this.dist - Y * 100}%`
        keyButtonContainer.style.left = `${X * 100}%`
        keyButtonContainer.style.right = `${100 - this.dist - X * 100}%`
        
        let keyButton = document.createElement("button");
        
        keyButton.classList += "button";

        for (let i = 0; i < 9; i++) {
            let key = keyset[i];
            let isRepeater = i == 4 && X == 1/3 && Y == 1/3;
            keyButton.addEventListener(this.inputType(i), this.getAction(key, isRepeater));
        }

        keyButton.addEventListener(this.inputType(9), this.getAction(keyset[9]));

        keyButton.addEventListener("mouseover", async () => {
            KeyLegend.currentButtonHover[buttonNum] = true;
        });
        keyButton.addEventListener("mouseleave", () => {
            KeyLegend.currentButtonHover[buttonNum] = false;
        });

        keyButtonContainer.appendChild(keyButton);

        return keyButtonContainer;
    }

    static inputType(index){
        switch (index) {
            case 0:
                return "dragtopleft";
            case 1:
                return "dragtop";
            case 2:
                return "dragtopright";
            case 3:
                return "dragleft";

            case 5:
                return "dragright";
            case 6:
                return "dragbottomleft";
            case 7:
                return "dragbottom";
            case 8:
                return "dragbottomright";

            case 9:
                return "mousehold";
            default:
                return "mouseclick";
        }
    }

    static getAction(key, shouldIgnoreSetRepeater){
        let act = (() => {
            if(!shouldIgnoreSetRepeater) this.setRepeater(key);
            SymbolActionPair.getAction(key)()
            ;
        });
        return act;
    }

    static setRepeater(key){
        let legendContainer = document.querySelector("#keylegendcontainer");
        let centerKeyLegend = legendContainer.childNodes[4];
        let repeaterkeyLegend = centerKeyLegend.childNodes[4];
        let buttonContainer = document.querySelector("#buttonscontainer");
        let repeaterKeyButton = buttonContainer.childNodes[4].childNodes[0];

        let sym = KeyLegend.generateSymbol(key, 4, 4, 1/3, 1/3);
        centerKeyLegend.replaceChild(sym, repeaterkeyLegend)


        if(KeyButton.repeaterFunc != null)
            repeaterKeyButton.removeEventListener("mouseclick", KeyButton.repeaterFunc);

        KeyButton.repeaterFunc = this.getAction(key, true);
        repeaterKeyButton.addEventListener("mouseclick", KeyButton.repeaterFunc);
    }
}