const inchInPixelsApproximantion = 96;
const holdThresholdInMilis = 500;
export class InteractionTypeDispatcher {

    static instance; 

    posWhenMouseDownX = 0;
    posWhenMouseDownY = 0;

    mousePosNowX;
    mousePosNowY;

    target;
    body;
    
    isDown = false;
    mouseDownTime;

    predictedAction = 0;

    constructor () {
        InteractionTypeDispatcher.instance = this;
        this.body = document.querySelector("body");
        this.body.addEventListener("mousedown", this.setPosMouseDown);
        this.body.addEventListener("mousemove", this.setNowMousePos);
        this.body.addEventListener("mouseup", this.commitAction);

        setInterval(this.actionPredictProxy, 13);
    }

    setPosMouseDown(e){
        InteractionTypeDispatcher.instance.isDown = true;

        InteractionTypeDispatcher.instance.posWhenMouseDownX = e.pageX;
        InteractionTypeDispatcher.instance.posWhenMouseDownY = e.pageY;
        InteractionTypeDispatcher.instance.target = e.target;
        InteractionTypeDispatcher.instance.mouseDownTime = new Date().getTime();
    }

    setNowMousePos(e) {
        InteractionTypeDispatcher.instance.mousePosNowX = e.pageX;
        InteractionTypeDispatcher.instance.mousePosNowY = e.pageY;
    }

    actionPredictProxy() {
        let isDown = InteractionTypeDispatcher.instance.isDown;
        if(!isDown) return;

        InteractionTypeDispatcher.instance.actionPredict();
    }

    actionPredict(){
        let X = InteractionTypeDispatcher.instance.mousePosNowX - InteractionTypeDispatcher.instance.posWhenMouseDownX;
        let Y = InteractionTypeDispatcher.instance.mousePosNowY - InteractionTypeDispatcher.instance.posWhenMouseDownY;

        let mouseMagnitudeSq = X*X + Y*Y;
        let mouseAngle = Math.atan2(Y, X);
        
        let inchinpix = inchInPixelsApproximantion;
        let magthresh = inchinpix * inchinpix / 400.0;
        let holdthresh = holdThresholdInMilis;
        //dispatch relevant
        if(mouseMagnitudeSq < magthresh){
            let time = new Date().getTime() - InteractionTypeDispatcher.instance.mouseDownTime;
            if(time > holdthresh){
                InteractionTypeDispatcher.instance.predictedAction = 9;
            }else{
                InteractionTypeDispatcher.instance.predictedAction = 0;
            }
            return;
        }
        
        InteractionTypeDispatcher.instance.predictedAction = 1 + Math.floor(((((mouseAngle / Math.PI) + 1) / 2) + 15/16) % 1.00 * 8);
    }

    commitAction(e){

        InteractionTypeDispatcher.instance.isDown = false;

        let parentParentID = (target) => {
            if(typeof target === 'undefined') return "";
            if(target === null) return "";
            return target.parentNode.parentNode.id;
        }

        if(parentParentID(InteractionTypeDispatcher.instance.target) != "buttonscontainer" && parentParentID(e.target) != "buttonscontainer") return;

        let eventName;

        switch (InteractionTypeDispatcher.instance.predictedAction){
            case 1:
                eventName = "dragtopleft";
                break;
            case 2:
                eventName = "dragtop";
                break;
            case 3:
                eventName = "dragtopright";
                break;
            case 4:
                eventName = "dragright";
                break;
            case 5:
                eventName = "dragbottomright"; 
                break;
            case 6:
                eventName = "dragbottom";
                break;
            case 7:
                eventName = "dragbottomleft";
                break;
            case 8:
                eventName = "dragleft";
                break;
            case 9: 
                eventName = "mousehold";
                break;
            default:
                eventName = "mouseclick";
                break;
        }

        InteractionTypeDispatcher.instance.target.dispatchEvent(new Event(eventName));
        InteractionTypeDispatcher.instance.predictedAction = 0;
    }
}