import { Action } from "./Action.js"

export class SymbolActionPair {
    static getSymbol(key){
        if(typeof key === 'string'){
            let textcontainer = document.createElement("span");
            textcontainer.innerHTML = key;
            return textcontainer;
        }else{
            let imageContainer = document.createElement("div");
            
            let imgnormal = document.createElement("img");
            let imghighlighted = document.createElement("img");
            
            imgnormal.style.position = "absolute";
            imghighlighted.style.position = "absolute";

            imgnormal.style.top = "0px";
            imgnormal.style.right = "0px";
            imghighlighted.style.top = "0px";
            imghighlighted.style.right = "0px";

            imgnormal.id = "normal";
            imghighlighted.id = "highlight"
            
            imghighlighted.style.visibility =  "hidden";
            
            imgnormal.src = `./images/${key.img}.png`;
            imghighlighted.src = `./images/${key.img}_highlight.png`;
            
            imageContainer.appendChild(imgnormal);
            imageContainer.appendChild(imghighlighted);
            
            return imageContainer;
        }
    }

    static getAction(key){
        if(typeof key === 'string'){
            return () => { Action.write(key); };
        }else{
            return () => { 
                let actionFunction = new Function('Action', `Action.${key.action}`); 
                return actionFunction(Action) 
            };
        }
    }
}