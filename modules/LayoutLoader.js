
import { KeyLegend } from "./KeyLegend.js";
import {KeyButton} from "./KeyButton.js";

function XOR(a, b){
    return ( a && !b ) || ( !a && b ); 
}

export class LayoutLoader {
    static layouts;
    static currentLayoutName;

    static shiftToggle = false;
    static capsToggle = false;
    static symbolToggle = false;
    
    static async init() {
        fetch('./layouts.json')
        .then((response) => response.json())
        .then((json) => LayoutLoader.layouts = JSON.parse(JSON.stringify(json)));

        
        console.log("Begin loading layouts...");
        let then = Date.now();
        while(LayoutLoader.layouts == null){
            let now = Date.now();
            if (now - then > 200) {
                await new Promise(resolve => setTimeout(resolve));
                then = now;
            }
        }
        
        console.log("layouts done loading!");
    }

    static async render(){
        
        let then = Date.now();
        while(LayoutLoader.layouts == null){
            let now = Date.now();
            if(now - then > 200) {
                await new Promise(resolve => setTimeout(resolve));
                then = now;
            }
        }

        if(LayoutLoader.symbolToggle) LayoutLoader.renderGivenName("symbols");
        else {
            if(XOR(LayoutLoader.shiftToggle, LayoutLoader.capsToggle)) LayoutLoader.renderGivenName("uppercase");
            else LayoutLoader.renderGivenName("default");
        }
    }

    static renderGivenName(name){
        
        let keyLegendContainer = document.querySelector("#keylegendcontainer");
        let buttonContainer = document.querySelector("#buttonscontainer");

        for (let y = 0; y < 3; y++) {
            for (let x = 0; x < 3; x++) {
                let keyIndex = y*3 + x;
                let keyset = LayoutLoader.layouts[name][keyIndex]; 
                let keyLegend = KeyLegend.render(keyset, keyIndex, x/3, y/3);
                keyLegendContainer.appendChild(keyLegend);   
                
                let button = KeyButton.render(keyset, keyIndex, x/3, y/3);
                
                buttonContainer.appendChild(button); 
            }   
        }
        
        console.log(`${name} layout loaded!`);
        LayoutLoader.currentLayoutName = name;
    }
}

LayoutLoader.init();