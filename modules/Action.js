import { LayoutLoader } from "./LayoutLoader.js";

export class Action {
    static write(text){
        let textarea = document.querySelector("textarea");
        if (text.match(/[a-z]/i)) {
            LayoutLoader.shiftToggle = false;
            LayoutLoader.render();
        }
        textarea.value += text;
    }

    static backspace(){
        let textarea = document.querySelector("textarea");
        textarea.value = textarea.value.substring(0, textarea.value.length - 1);
    }

    static newLine(){
        this.write('\n');
    }

    static ToggleSymbol(){
        LayoutLoader.symbolToggle = !LayoutLoader.symbolToggle;
        LayoutLoader.render()
    }

    static toggleShift(){
        LayoutLoader.shiftToggle = !LayoutLoader.shiftToggle;
        LayoutLoader.render()
    }

    static toggleCaps(){
        LayoutLoader.capsToggle = !LayoutLoader.capsToggle;
        LayoutLoader.render()
    }
}