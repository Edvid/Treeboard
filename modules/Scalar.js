const scalarStep = 1.02;

export class Scalar {
    static createScalars() {

        let scalardiv = document.querySelectorAll(".scalar");

        scalardiv.forEach(div => {

            div.style.margin = "15px"

            let plus = document.createElement("button");
            let minus = document.createElement("button");

            plus.classList += "button";
            minus.classList += "button";

            plus.style.width = ".3in";
            plus.style.height = ".2in";
            minus.style.width = ".3in";
            minus.style.height = ".2in";

            plus.innerText = '+';
            minus.innerText = '-';

            
            let idpointer = div.id.replace('scalar', '');

            let scalingElement = document.querySelector(`#${idpointer}`);

            plus.addEventListener("click", () => {

                let boundingBox = scalingElement.getBoundingClientRect();
                let currentWidth = boundingBox.width;
                let currentHeight = boundingBox.height;

                scalingElement.style.width = `${currentWidth * scalarStep}px`;
                scalingElement.style.height = `${currentHeight * scalarStep}px`;
            });

            minus.addEventListener("click", () => {

                let boundingBox = scalingElement.getBoundingClientRect();
                let currentWidth = boundingBox.width;
                let currentHeight = boundingBox.height;

                scalingElement.style.width = `${currentWidth / scalarStep}px`;
                scalingElement.style.height = `${currentHeight / scalarStep}px`;
            });


            div.appendChild(plus);
            div.appendChild(minus);
        });
    }
}

