import {SymbolActionPair} from "./SymbolActionPair.js";
import {InteractionTypeDispatcher} from "./InteractionTypeDispatcher.js";

const highlightColor = "chartreuse";
export class KeyLegend {

    static dist = 33.333;

    static currentButtonHover = [];
    static currentLegendHighlight = [];

    static render(keyset, legendNum, X, Y){

        let keyLegend = document.createElement("div");

        keyLegend.classList += "legend";

        keyLegend.style.top = `${Y * 100}%`
        keyLegend.style.bottom = `${100 - this.dist - Y * 100}%`
        keyLegend.style.left = `${X * 100}%`
        keyLegend.style.right = `${100 - this.dist - X * 100}%`


        let symNum = 0;

        for (let y = 0; y < 3; y++) {
            for (let x = 0; x < 3; x++) {
                let key = keyset[y*3 + x];
                keyLegend.appendChild(this.generateSymbol(key, legendNum, symNum++, x/3, y/3));
            }    
        }

        keyLegend.appendChild(this.generateSymbol(keyset[9], legendNum, symNum, 0.5, 0.5));

        setInterval(() => {
            if(InteractionTypeDispatcher.instance.isDown) return; 
            KeyLegend.highlightLegend(keyLegend, legendNum);
        }, 13);

        KeyLegend.currentButtonHover.push(false);
        KeyLegend.currentLegendHighlight.push(false);

        return keyLegend;
    }

    static generateSymbol(key, legendNum, symNym, x, y){
        let sym = SymbolActionPair.getSymbol(key);

        sym.style.top = `${y * 100}%`
        sym.style.bottom = `${100 - this.dist - y * 100}%`
        sym.style.left = `${x * 100}%`
        sym.style.right = `${100 - this.dist - x * 100}%`

        setInterval(() => {
            KeyLegend.highlightSymbol(sym, legendNum, symNym); 
        }, 13);

        return sym;
    }

    static highlightLegend(element, legendNum){
        KeyLegend.currentLegendHighlight[legendNum] = KeyLegend.currentButtonHover[legendNum];
        let shouldHighlight = KeyLegend.currentLegendHighlight[legendNum];

        if(shouldHighlight){
            element.style.borderColor = highlightColor;
        }else{
            element.style.borderColor = "rgb(44, 41, 54)";
        }
    }

    static highlightSymbol(element, legendNum, symNym){ 
        let shouldHighlight = 
            KeyLegend.layoutNumToActionNum(symNym) == InteractionTypeDispatcher.instance.predictedAction &&
            KeyLegend.currentLegendHighlight[legendNum];

        if(element.tagName == 'DIV'){
            let imgnormal = element.childNodes[0]
            let imghighlight = element.childNodes[1];
            imgnormal.style.visibility = shouldHighlight ? "hidden" : "";
            imghighlight.style.visibility = shouldHighlight ? "" : "hidden";
        }else{
            element.style.color = shouldHighlight ? highlightColor : "inherit";
        }
    }

    static layoutNumToActionNum(symNum){
        switch (symNum) {
            case 0:
                return 1;
            case 1:
                return 2;
            case 2:
                return 3;
            case 3:
                return 8;
            case 4:
                return 0;
            case 5:
                return 4;
            case 6:
                return 7;
            case 7:
                return 6;
            case 8:
                return 5;
            default:
                return symNum
        }
    }
}