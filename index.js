
import {InteractionTypeDispatcher} from "./modules/InteractionTypeDispatcher.js";
import {Scalar} from "./modules/Scalar.js";
import { LayoutLoader } from "./modules/LayoutLoader.js";

let body = document.querySelector("body");

async function run(){
    new InteractionTypeDispatcher();
    Scalar.createScalars();

    LayoutLoader.render();

    let clipboardbutton = document.querySelector(".button#clipboard-btn");
    clipboardbutton.addEventListener("click", () => {
        let textarea = document.querySelector("textarea");
        textarea.select();
        textarea.setSelectionRange(0, 99999);
        navigator.clipboard.writeText(textarea.value);
    });
    
}

document.addEventListener("DOMContentLoaded", run);