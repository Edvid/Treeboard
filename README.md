# Treeboard

A keyboard is a board of keys. This is a tree of keys.
A very small on-screen keyboard is at the bottom center of the screen. Clicking, holding, or click & dragging any of these buttons will do each their thing.

## Some terminology
Textarea - where your typed text will appear. The tiny button next to it lets you copy its content into the clipboard.\
Key Legend Area - The place you'll see what the buttons do, as the buttons themselves are too small to explain\
Key Legend - One of the 9 squares inside the Key Legend Area, which itself has up to 10 segments for the 10 actions the correspónding button can perform.
Key Button Area - the much smaller area of actual buttons to press.\
Layout - The data structure that defines which buttons do which things given which of clicking, holding and dragging was done.\
Layout Designer - For now, just me. But I intend to add functionality that allows you to upload your own JSON layouts sometime in the future.
## How it works

### Inputs

The Key Button Area is where the you will be inputting with your mouse by clicking, holding, or clicking and dragging your mouse around. You can see which buttons do what in the Key Legend Area. The segments in a Key Legend can have either plain text for text inputs or an image icon for more complex/special actions. NOTE: Not yet implemented ~~A few buttons such as shift, caps, and symbol button found in the center Key Legend can change the layout to allow for capitalized versions of the letters, or more symbols.~~

the current layout has the following special actions.

- Symbol - A layout exchanger that will allow for many more types of symbols
- Autocomplete menu - Allow you to swiftly select autocomplete suggestions via the treeboard rather than move your mouse far away.
- Shift - Self explanatory
- Backspace - Self explanatory
- Repeat - Repeats the previous action
- Enter - Inputs a new line in the text
- Caps - Toggles Caps

Note that some buttons are greyed out. They have no functionality as of now. This site is still in early development.

### How to read a Key Legend

If the Key Legend Area displays a button this way

```md
+---------+
| 1  2  3 |
| 4  5  6 |
|     0   |
| 7  8  9 |
+---------+
```

It means that

- If pressed, you type `5`
- If Held for 0.5 seconds, you type `8`
- If held and dragged top left, you type `1`
- If held and dragged up, you type `2`
- If held and dragged top right, you type `3`
- If held and dragged left, you type `4`
- If held and dragged right, you type `6`
- If held and dragged bottom left, you type `7`
- If held and dragged down, you type `8`
- If held and dragged bottom right, you type `9`
